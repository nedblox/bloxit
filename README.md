# bloxit

Quick install Docker and DFP (on Docker) for Ubunutu

Steps:

 
1. Create your on-prem host in the CSP portal and get the API access key

2. Install git and pull the script

```
sudo apt install git -y && git clone https://gitlab.com/nedblox/bloxit.git
```

3. Execute the setup.sh script in the ./bloxit/ directory, and input the API access key when prompted

```
sudo sh bloxit/setup.sh
```
