#!/bin/bash
#KEY=${1?Error: no API key given}
if [ -z "$(docker images infobloxcto/onprem.agent)" ]; then
    if [ -z "$(docker version)" ]; then
        apt install curl -y
        curl -fsSL https://get.docker.com -o get-docker.sh
        sh get-docker.sh
        usermod -aG docker $USER
    else
        mkdir BloxOne
        cd BloxOne
        wget http://ib-noa-prod.csp.infoblox.com.s3-website-us-east-1.amazonaws.com/BloxOne_OnPrem_Docker_2.0.11.tar.gz
        docker load -i BloxOne_OnPrem_Docker_2.0.11.tar.gz
        echo -e "\n \n"
        OPVERSION=v2.0.11
    fi
else
    OPVERSION=$(docker images --filter=reference='infobloxcto/onprem.agent:v*' --format "{{.Tag}}" | sort |tail -1)
fi

read -p "Enter API Key: " KEY
docker run -d --name blox.noa --restart=always --net=host -v /var/run/docker.sock:/var/run/docker.sock infobloxcto/onprem.agent:$OPVERSION --access-key=$KEY

sleep 5s
if [-z "$(docker ps --filter "name=blox.noa")"]; then
    echo "\n \n Deployment FAILED"
else
    echo "\n \n Deployment Succeded"